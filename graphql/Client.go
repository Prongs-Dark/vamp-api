package graphql

import (
	"context"
	"strconv"
	"strings"
	"time"
	priceprovider "vamp-api/price-provider"
	"vamp-api/token"
	"vamp-api/util/errors"

	"github.com/hasura/go-graphql-client"
	"github.com/sirupsen/logrus"
)

// GetClient will return a different graphql client depending on the provider
func GetClient(provider *priceprovider.PriceProvider) *graphql.Client {
	switch {
	case provider.Id == 1:
		return graphql.NewClient("https://api.thegraph.com/subgraphs/name/eerieeight/spookyswap", nil)
	default:
		return nil
	}
}

func GetSpookyTokenPrice(client *graphql.Client, token *token.Token) *float64 {
	c, cancelCtx := context.WithTimeout(context.Background(), time.Duration(5)*time.Second)
	defer cancelCtx()

	params := map[string]interface{}{
		"address": graphql.ID(strings.ToLower(token.Address)),
	}

	var query struct {
		Token struct {
			Id           graphql.String
			TokenDayData []struct {
				PriceUSD graphql.String `graphql:"priceUSD"`
			} `graphql:"tokenDayData(orderBy:date,first:1,orderDirection:desc)"`
		} `graphql:"token(id: $address)"`
	}

	err := client.Query(c, &query, params)
	errors.HandleCliError(errors.WrapToCliError(err, errors.MAJOR))

	if len(query.Token.TokenDayData) == 0 {
		logrus.Errorln("Could not retrieve data for " + token.Ticker)
		return nil
	}

	priceString := string(query.Token.TokenDayData[0].PriceUSD)
	price, err := strconv.ParseFloat(priceString, 64)
	errors.HandleCliError(errors.WrapToCliError(err, errors.MAJOR))

	return &price
}
