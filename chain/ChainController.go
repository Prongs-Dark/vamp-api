package chain

import (
	"context"
	"net/http"
	"strconv"
	"time"
	"vamp-api/database"
	"vamp-api/util/errors"

	"github.com/gin-gonic/gin"
)

func AddRoutes(api *gin.RouterGroup) {
	chainRoutes := api.Group("/chains")

	chainRoutes.GET("/:id", addNewChain)
}

func addNewChain(ctx *gin.Context) {
	c, cancelCtx := context.WithTimeout(context.Background(), time.Duration(1500)*time.Millisecond)
	defer cancelCtx()

	id, err := strconv.ParseInt(ctx.Param("id"), 0, 32)
	shouldStop := errors.HandleRestError(errors.WrapToRestError(err, "The id you sent is not a number", http.StatusBadRequest), ctx)
	if shouldStop {
		return
	}

	chain := new(Chain)

	err = database.Database.NewSelect().
		Model(chain).
		Relation("Tokens").
		Where("id = ?", id).
		Scan(c)
	shouldStop = errors.HandleRestError(errors.WrapToRestError(err, "Couldn't retrieve the cahin", http.StatusInternalServerError), ctx)
	if shouldStop {
		return
	}

	ctx.JSON(http.StatusOK, chain)
}
