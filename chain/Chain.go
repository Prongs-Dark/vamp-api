package chain

import (
	"time"
	"vamp-api/token"
)

type Chain struct {
	Id        int            `bun:",pk" json:"id"`
	Name      string         `json:"name"`
	Tokens    []*token.Token `bun:"rel:has-many,join:id=chain_id" json:"tokens"`
	CreatedAt time.Time      `bun:",nullzero,notnull,default:current_timestamp" json:"-"`
	UpdatedAt time.Time      `bun:",nullzero,notnull,default:current_timestamp" json:"-"`
}
