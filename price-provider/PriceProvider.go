package priceprovider

import (
	"time"
)

type PriceProvider struct {
	Id        int       `bun:",pk" json:"id"`
	Name      string    `json:"name"`
	URL       string    `json:"url"`
	CreatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp" json:"-"`
	UpdatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp" json:"-"`
}
