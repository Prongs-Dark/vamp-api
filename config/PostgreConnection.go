package config

import (
	"context"
	"database/sql"
	"os"
	"time"
	"vamp-api/chain"
	"vamp-api/database"
	mixedmodels "vamp-api/mixed-models"
	priceprovider "vamp-api/price-provider"
	"vamp-api/token"
	"vamp-api/util/errors"

	"github.com/sirupsen/logrus"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dbfixture"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
)

var ctx, cancelCtx = context.WithTimeout(context.Background(), time.Duration(1500)*time.Millisecond)

func InitializePostgreDatabase() {
	dsn := "postgres://vamp-dev:vamp-dev@localhost:5432/vampire-finance?sslmode=disable"

	sqldb := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))

	db := bun.NewDB(sqldb, pgdialect.New())

	createPostgreTables(db)

	logrus.Infoln("Database initialized")
}

func createPostgreTables(db *bun.DB) {
	defer cancelCtx()

	db.RegisterModel((*mixedmodels.TokenHasPriceProvider)(nil))
	db.RegisterModel((*chain.Chain)(nil))
	db.RegisterModel((*priceprovider.PriceProvider)(nil))
	db.RegisterModel((*token.Token)(nil))

	createPostgreTable(db, (*chain.Chain)(nil), "Chain")
	createPostgreTable(db, (*token.Token)(nil), "Token")
	createPostgreTable(db, (*priceprovider.PriceProvider)(nil), "PriceProvider")
	createPostgreTable(db, (*mixedmodels.TokenHasPriceProvider)(nil), "TokenHasPriceProvider")

	populateDatabase(db)

	logrus.Infoln("All models created successfully")

	database.Database = db
}

func createPostgreTable(db *bun.DB, model interface{}, modelName ...string) {
	_, err := db.NewCreateTable().
		Model(model).
		Varchar(250).
		IfNotExists().
		Exec(ctx)

	errors.HandleCliError(errors.WrapToCliError(err, errors.BREAKING))

	if modelName[0] != "" {
		logrus.Infoln("Created model " + modelName[0])
	}
}

func populateDatabase(db *bun.DB) {
	logrus.Infoln("Starting populating the database")

	fixture := dbfixture.New(db, dbfixture.WithTruncateTables())

	err := fixture.Load(ctx, os.DirFS("fixtures"), "initial.yaml")

	errors.HandleCliError(errors.WrapToCliError(err, errors.BREAKING))

	logrus.Infoln("Database populated successfully")
}
