package mixedmodels

import (
	"context"
	priceprovider "vamp-api/price-provider"
	"vamp-api/token"

	"github.com/uptrace/bun"
)

type TokenHasPriceProvider struct {
	TokenId         string                       `bun:",pk"`
	PriceProviderId int                          `bun:",pk"`
	Token           *token.Token                 `bun:"rel:belongs-to,join:token_id=address"`
	PriceProvider   *priceprovider.PriceProvider `bun:"rel:belongs-to,join:price_provider_id=id"`
	Main            *bool
}

func (*TokenHasPriceProvider) BeforeCreateTable(ctx context.Context, query *bun.CreateTableQuery) error {
	query.ForeignKey(`("token_id") REFERENCES "tokens" ("address") ON DELETE CASCADE ON UPDATE CASCADE`)
	query.ForeignKey(`("price_provider_id") REFERENCES "price_providers" ("id") ON DELETE CASCADE ON UPDATE CASCADE`)
	return nil
}
