# Production checklist:

 1. Check config for database
 2. Change the passwords for docker containers
 3. Disable APIs that are not meant for public consumption