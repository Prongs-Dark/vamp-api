package errors

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func HandleCliError(cliError CliError) {
	if cliError.Err != nil {
		switch cliError.ErrorType {
		case BREAKING:
			logrus.Fatalln(cliError.Err)
		case MAJOR:
			logrus.Errorln(cliError.Err)
		case MINOR:
			logrus.Warnln(cliError.Err)
		}
	}
}

func HandleRestError(restError RestError, context *gin.Context) bool {
	if restError.Err != nil {
		logrus.Warnln(restError.Err)
		context.AbortWithStatusJSON(restError.Status, UnwrapRestErrorToErrorResponse(restError))
		return true
	}

	return false
}
