package errors

type ErrorType int64

const (
	BREAKING ErrorType = iota // this error terminates the program (use for preventing program startup)
	MAJOR                     // something bad happened
	MINOR                     // somethign potentially bad happened
)
