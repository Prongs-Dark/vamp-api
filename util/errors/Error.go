package errors

// Base error struct, wraps a GO error.
type Error struct {
	Err error
}

// Error meant to be displayed only in the CLI of the program
type CliError struct {
	Error
	ErrorType ErrorType
}

// Error meant to be returned as part of a REST API response
type RestError struct {
	Error
	Message string
	Status  int
}

type ErrorResponse struct {
	Message string `json:"message"`
}

func WrapToCliError(err error, errorType ErrorType) CliError {
	return CliError{
		Error:     Error{Err: err},
		ErrorType: errorType,
	}
}

func WrapToRestError(err error, message string, status int) RestError {
	return RestError{
		Error:   Error{Err: err},
		Message: message,
		Status:  status,
	}
}

func UnwrapRestErrorToErrorResponse(restError RestError) ErrorResponse {
	return ErrorResponse{
		Message: restError.Message,
	}
}
