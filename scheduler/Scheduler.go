package scheduler

import (
	"context"
	"time"
	"vamp-api/database"
	"vamp-api/graphql"
	mixedmodels "vamp-api/mixed-models"
	priceprovider "vamp-api/price-provider"
	"vamp-api/token"
	"vamp-api/util/errors"

	"github.com/go-co-op/gocron"
	"github.com/sirupsen/logrus"
)

func Schedule() {
	s := gocron.NewScheduler(time.UTC)
	s.Every(10).Minutes().Do(refreshTokens)
	s.SingletonMode().StartAsync()
}

func refreshTokens() {
	startTime := time.Now()
	logrus.Infoln("Strated job for retrieving price data -", startTime.UTC())

	tokens := retrieveAllTokens()
	refreshPriceData(tokens)

	endTime := time.Now()
	duration := (endTime.Sub(startTime).Seconds())
	logrus.Infoln("Finished job for retrieving price data -", endTime.UTC(), " (took", duration, "seconds)")
}

func retrieveAllTokens() []*token.Token {
	logrus.Infoln("Retrieving tokens...")

	c, cancelCtx := context.WithTimeout(context.Background(), time.Duration(1500)*time.Millisecond)
	defer cancelCtx()

	tokens := make([]*token.Token, 0)

	err := database.Database.NewSelect().
		Model(&tokens).
		Relation("PriceProviders").
		Scan(c)
	errors.HandleCliError(errors.WrapToCliError(err, errors.MAJOR))

	return tokens
}

func refreshPriceData(tokens []*token.Token) {
	logrus.Infoln("Refreshing prices...")

	for i := 0; i < len(tokens); i++ {
		provider := filterForMainProvider(tokens[i])
		if provider == nil {
			logrus.Warningln("The token " + tokens[i].Address + " does not have a main price provider")
			continue
		}

		client := graphql.GetClient(provider)
		price := graphql.GetSpookyTokenPrice(client, tokens[i])

		tokens[i].Price = price
	}

	c, cancelCtx := context.WithTimeout(context.Background(), time.Duration(1500)*time.Millisecond)
	defer cancelCtx()

	_, err := database.Database.NewUpdate().
		Model(&tokens).
		Bulk().
		Exec(c)
	errors.HandleCliError(errors.WrapToCliError(err, errors.MAJOR))
}

func filterForMainProvider(token *token.Token) *priceprovider.PriceProvider {
	providers := token.PriceProviders

	for _, provider := range providers {
		c, cancelCtx := context.WithTimeout(context.Background(), time.Duration(1500)*time.Millisecond)

		tokenHasPriceProvider := new(mixedmodels.TokenHasPriceProvider)

		err := database.Database.NewSelect().
			Model(tokenHasPriceProvider).
			Where("token_id = ? AND price_provider_id = ?", token.Address, provider.Id).
			Scan(c)
		errors.HandleCliError(errors.WrapToCliError(err, errors.MAJOR))
		if err != nil {
			cancelCtx()
			continue
		}

		if *tokenHasPriceProvider.Main {
			cancelCtx()
			return provider
		}

		cancelCtx()
	}

	return nil
}
