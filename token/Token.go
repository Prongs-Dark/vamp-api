package token

import (
	"context"
	"time"
	priceprovider "vamp-api/price-provider"

	"github.com/uptrace/bun"
)

type Token struct {
	Address        string                         `bun:",pk" json:"address"`
	Name           string                         `json:"name"`
	Ticker         string                         `json:"ticker"`
	Decimals       int                            `json:"decimals"`
	Price          *float64                       `json:"price"`
	ChainId        int                            `json:"chainId"`
	PriceProviders []*priceprovider.PriceProvider `bun:"m2m:token_has_price_providers,join:Token=PriceProvider" json:"provider,omitempty"`
	CreatedAt      time.Time                      `bun:",nullzero,notnull,default:current_timestamp" json:"-"`
	UpdatedAt      time.Time                      `bun:",nullzero,notnull,default:current_timestamp" json:"-"`
}

func (*Token) BeforeCreateTable(ctx context.Context, query *bun.CreateTableQuery) error {
	query.ForeignKey(`("chain_id") REFERENCES "chains" ("id") ON DELETE CASCADE ON UPDATE CASCADE`)
	return nil
}
