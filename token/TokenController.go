package token

import (
	"context"
	"net/http"
	"time"
	"vamp-api/database"
	"vamp-api/util/errors"

	"github.com/gin-gonic/gin"
)

func AddTokenRoutes(api *gin.RouterGroup) {
	tokenGroup := api.Group("/tokens")

	tokenGroup.GET("/:id", getTokens)
}

func getTokens(ctx *gin.Context) {
	c, cancelCtx := context.WithTimeout(context.Background(), time.Duration(1500)*time.Millisecond)
	defer cancelCtx()

	id := ctx.Param("id")

	token := new(Token)

	err := database.Database.NewSelect().
		Model(token).
		Relation("PriceProviders").
		Where("address = ?", id).
		Scan(c)
	shouldStop := errors.HandleRestError(errors.WrapToRestError(err, "Couldn't retrieve the token", http.StatusInternalServerError), ctx)
	if shouldStop {
		return
	}

	ctx.JSON(http.StatusOK, token)
}
