package main

import (
	"vamp-api/chain"
	"vamp-api/config"
	priceprovider "vamp-api/price-provider"
	"vamp-api/scheduler"
	"vamp-api/token"
	"vamp-api/util/errors"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	v1_api := r.Group("/v1")
	token.AddTokenRoutes(v1_api)
	priceprovider.AddRoutes(v1_api)
	chain.AddRoutes(v1_api)

	config.InitializePostgreDatabase()

	scheduler.Schedule()

	err := r.Run("0.0.0.0:1505") // listen and serve on 0.0.0.0:1505
	errors.HandleCliError(errors.WrapToCliError(err, errors.BREAKING))
}
